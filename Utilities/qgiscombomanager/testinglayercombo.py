from PyQt4.QtCore import Qt, QObject, pyqtSignal
import string
from os import listdir
from os.path import isfile, join

def remove_accents(data):
    return filter(lambda char: char in string.ascii_uppercase, data.upper())


class LayerCombo(QObject):
    def __init__(self, widget, datapath):
        QObject.__init__(self, widget)
        self.widget = widget
        self.datapath = datapath
        self.files = None
        self.currentItem = None

        # get layers in path and populate combobox
        self.__getItems()

    def changed_selection(self):
        """set current item in widget"""

        self.currentItem = self.widget.currentText()


    def __getItems(self):
        """get all tif files located in given path and populate combobox"""

        self.files = [f for f in listdir(self.datapath) if isfile(join(self.datapath, f))]
        self.tiffiles = [f for f in self.files if f[-4:] == '.tif']
        self.widget.clear()
        for item in self.tiffiles:
            self.widget.addItem(item)
        self.widget.setCurrentIndex(-1)


class RasterLayerCombo(LayerCombo):
    def __init__(self, widget, datapath):
        LayerCombo.__init__(self, widget, datapath)
