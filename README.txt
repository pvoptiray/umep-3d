This is the official repository for the Urban Multi-scale Universal Predictor. 
UMEP includes various climate sensitive planning tools and comes as a plugin 
for QGIS (www.qgis.org). UMEP will eventually be available from the QGIS 
official plugin repository but in the mean time, it can be downloaded from here
or from our own plugin repository. 

Instructions on how to install UMEP can be found at:
http://urban-climate.net/umep/UMEP_Manual#UMEP:_Getting_Started

Join our email list to keep updated:
http://www.lists.rdg.ac.uk/mailman/listinfo/met-umep 

Visit out website:
http://www.urban-climate.net/umep/

Contact: Fredrik Lindberg, fredrikl[a]gvc.gu.se


